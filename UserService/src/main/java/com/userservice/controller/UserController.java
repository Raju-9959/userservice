package com.userservice.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.userservice.commonutils.Constants;
import com.userservice.jpa.User;
import com.userservice.services.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("users")
@RefreshScope
@Api(value = "User Resource")
@ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully added"),
        @ApiResponse(code = 200, message = "The request has been succeeded "),
        @ApiResponse(code = 500, message = "Internal Server error"),
        @ApiResponse(code = 404, message = "Thre resource is not found"),
        @ApiResponse(code = 400, message = "The requested resource is not allowed") })
@CrossOrigin(origins = "*",allowedHeaders = "*")
public class UserController {

	private static final Logger logger = LogManager.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	@Value("${test}")
	private String value;
	
	@GetMapping("property")
	public String getValue() {
		return value;
	}
	
	
	@GetMapping
    @ApiOperation(value = "Getting all the users", response = ResponseEntity.class)
	public ResponseEntity<Response> get() {
		logger.info("/users is called with get method");
		ResponseEntity<Response> re = null;
		List<User> users = userService.getAllUsers();
		if (users.isEmpty())
			re = new ResponseEntity<>(new Response(Constants.NO_USERS.getMsg(), null), HttpStatus.NO_CONTENT);
		else
			re = new ResponseEntity<>(new Response(Constants.SUCCESSFUL.getMsg(), users), HttpStatus.OK);

		return re;
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "Getting a  user", response = ResponseEntity.class)
	public ResponseEntity<Response> getUser(@PathVariable long id) {
		logger.info("/users/{id} is called get method");
		return new ResponseEntity<>(new Response(Constants.SUCCESSFUL.getMsg(), userService.getUser(id)), HttpStatus.OK);

	}

	@PostMapping
	@ApiOperation(value = "Add new user", response = ResponseEntity.class)
	public ResponseEntity<Response> addUser(@RequestBody User user) {
		logger.info("/users/ is called post method acceptiong Requestbody user");
		logger.info("Received user {}",user);
		ResponseEntity<Response> re = null;
		if (userService.validateUser(user)) {
			re = new ResponseEntity<>(new Response(Constants.CREATED.getMsg(), userService.addUser(user)), HttpStatus.CREATED);
		} else {
			re = new ResponseEntity<>(new Response(Constants.INVALID_PARAMERTES.getMsg(), null), HttpStatus.BAD_REQUEST);
		}
		return re;
	}

	@DeleteMapping("{id}")
	 @ApiOperation(value = "Delete a user", response = ResponseEntity.class)
	public ResponseEntity<Response> deleteUser(@PathVariable long id) {
		logger.info("users/{id} is called with delete method");
		User user = userService.getUser(id);// if no user is found then it throws NoUserFound exception
											// and sends 404 from controller advice
		userService.deleteUser(id);
		return new ResponseEntity<>(new Response(Constants.DELETED.getMsg(), null), HttpStatus.OK);
	}

	@PutMapping
	 @ApiOperation(value = "Update a user", response = ResponseEntity.class)
	public ResponseEntity<Response> updateUser(@RequestBody User user) {
		logger.info("/users is called with patchmapping accepting user RequesetBody");
		logger.info("received user: "+user);
		ResponseEntity<Response> re = null;
		if (userService.validateUser(user))
			re = new ResponseEntity<>(new Response(Constants.UPDATED.getMsg(), userService.updateUser(user)), HttpStatus.OK);
		else
			re = new ResponseEntity<>(new Response("successfully updated the user", null), HttpStatus.BAD_REQUEST);
		return re;
	}
	
}
