package com.userservice.services;

import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.userservice.exception.UserNotFoundException;
import com.userservice.jpa.User;
import com.userservice.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	static Logger logger = LogManager.getLogger(UserServiceImpl.class);

	@Autowired
	private UserRepository userRepo;

	@Override
	public List<User> getAllUsers() {
		logger.info("getAllUsers Method is called");
		return userRepo.findAll();
	}

	@Override
	public User getUser(long id) {
		logger.info("getuser Method is called with id {}", id);
		return userRepo.findById(id)
				.orElseThrow(() -> new UserNotFoundException("No Users Available with given id: " + id));
	}

	@Override
	public User addUser(User user) {
		logger.info("addUser method is called");
		return userRepo.save(user);
	}

	@Override
	public void deleteUser(long id) {
		logger.info("deleteUser method is called with id {}", id);
		userRepo.deleteById(id);
	}

	@Override
	public User updateUser(User user) {
		logger.info("updateUser method is called");
		Optional<User> userOpt = userRepo.findById(user.getId());
		if (userOpt.isPresent()) {
			return userRepo.save(user);
		} else {
			logger.info("No user found exception wit id " + user.getId());
			throw new UserNotFoundException("No user found with id : " + user.getId());
		}
	}

	@Override
	public boolean validateUser(User user) {
		logger.info("validateUser Method is called");
		boolean status = true;
		if (user.getName().equals("") || user.getEmail().equals("") || user.getPassword().equals(""))
			status = false;
		logger.info("validateUser  status:" + status);
		return status;

	}

}
