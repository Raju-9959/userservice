package com.userservice.services;

import java.util.List;

import com.userservice.jpa.User;

public interface UserService {

	public List<User> getAllUsers();

	public User getUser(long id);

	public User addUser(User user);

	public void deleteUser(long id);

	public User updateUser(User user);

	public boolean validateUser(User user);

}
