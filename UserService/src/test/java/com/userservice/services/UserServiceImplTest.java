package com.userservice.services;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.userservice.exception.UserNotFoundException;
import com.userservice.jpa.User;
import com.userservice.repository.UserRepository;

class UserServiceImplTest {

	@InjectMocks
	UserServiceImpl userService;

	@Mock
	UserRepository userRepo;

	static List<User> users;
	static List<User> newUsers;

	@BeforeEach
	void init() {
		MockitoAnnotations.initMocks(this);
	}

	@BeforeAll
	public static void setValues() {
		users = new ArrayList<>();
		newUsers = new ArrayList<>();
		users.add(new User(1l, "testName", "testEmail", "testPass"));
		users.add(new User(2l, "testName2", "testEmail2", "testPass2"));
		newUsers.addAll(users);
		newUsers.add(new User(3l, "testName3", "testEmail3", "testEmail4"));
	}

	@Test
	void testGetAllUsers() {
		when(userRepo.findAll()).thenReturn(users);
		Assertions.assertEquals(users, userService.getAllUsers());
	}

	@Test
	void testGetAllUsersNegative() {
		when(userRepo.findAll()).thenReturn(users);
		Assertions.assertNotEquals(newUsers, userService.getAllUsers());
	}

	@Test
	void testGetUser() {
		when(userRepo.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(users.get(0)));
		Assertions.assertEquals(users.get(0), userService.getUser(Mockito.anyLong()));

	}

	@Test
	void testGetUserNegative() {
		when(userRepo.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(users.get(0)));
		Assertions.assertNotEquals(users.get(1), userService.getUser(Mockito.anyLong()));

	}

	@Test
	void testGetUserNegativeException() {
		when(userRepo.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(null));
		Assertions.assertThrows(UserNotFoundException.class, () -> userService.getUser(Mockito.anyLong()));

	}

	@Test
	void testAddUser() {
		Mockito.when(userRepo.save(users.get(0))).thenReturn(users.get(0));
		Assertions.assertEquals(users.get(0), userService.addUser(users.get(0)));
	}

	@Test
	void testAddUserNegative() {
		Mockito.when(userRepo.save(Mockito.any(User.class))).thenReturn(users.get(0));
		Assertions.assertNotEquals(users.get(1), userService.addUser(Mockito.any(User.class)));
	}

	@Test
	void testDeleteUser() {
		Mockito.doNothing().when(userRepo).deleteById(Mockito.anyLong());
		userService.deleteUser(Mockito.anyLong());
		Mockito.verify(userRepo).deleteById(Mockito.anyLong());
	}

	@Test
	void testUpdateUser() {
		Mockito.when(userRepo.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(users.get(0)));
		Mockito.when(userRepo.save(users.get(0))).thenReturn(users.get(0));
		Assertions.assertEquals(users.get(0), userService.updateUser(users.get(0)));
	}

	@Test
	void testUpdateUserNegativeException() {
		when(userRepo.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(null));
		when(userRepo.save(users.get(0))).thenReturn(users.get(0));
		Assertions.assertThrows(UserNotFoundException.class, () -> userService.updateUser(users.get(0)));

	}

	@Test
	void testValidateUser() {
		User user=new User(5l,"test","test","test");
		Assertions.assertEquals(true, userService.validateUser(user));
	}

	@Test
	void testValidateUserName() {
		User user=new User(5l,"","test","test");
		Assertions.assertNotEquals(true, userService.validateUser(user));
	}

	@Test
	void testValidateUserEmail() {
		User user=new User(5l,"","test","test");
		Assertions.assertNotEquals(true, userService.validateUser(user));
	}

	
	@Test
	void testValidateUserPass() {
		User user=new User(5l,"","test","");
		Assertions.assertNotEquals(true, userService.validateUser(user));
	}

}
