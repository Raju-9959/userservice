package com.userservice.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.userservice.jpa.User;
import com.userservice.services.UserServiceImpl;

class UserControllerTest {

	private MockMvc mockMvc;

	@InjectMocks
	UserController userController;

	@Mock
	UserServiceImpl userService;

	static List<User> users;
	static List<User> newUsers;

	@BeforeEach
	void init() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
	}

	@BeforeAll
	public static void setValues() {
		users = new ArrayList<>();
		newUsers = new ArrayList<>();
		users.add(new User(1l, "testName", "testEmail", "testPass"));
		users.add(new User(2l, "testName2", "testEmail2", "testPass2"));
		newUsers.addAll(users);
		newUsers.add(new User(3l, "testName3", "testEmail3", "testEmail4"));
	}

	@Test
	void testGet() throws Exception {
		Mockito.when(userService.getAllUsers()).thenReturn(users);
		this.mockMvc.perform(get("/users")).andExpect(status().isOk());
	}

	@Test
	void testGetBody() {
		Mockito.when(userService.getAllUsers()).thenReturn(users);
		Assertions.assertEquals(users, userController.get().getBody().getBody());

	}

	@Test
	void testGetBodyNegative() {
		Mockito.when(userService.getAllUsers()).thenReturn(users);
		Assertions.assertNotEquals(newUsers, userController.get().getBody().getBody());

	}

	@Test
	void testGetUser() throws Exception {
		Mockito.when(userService.getUser(Mockito.anyLong())).thenReturn(users.get(0));
		this.mockMvc.perform(get("/users/1")).andExpect(status().isOk());
	}

	@Test
	void testGetUserBody() {
		Mockito.when(userService.getUser(Mockito.anyLong())).thenReturn(users.get(0));
		Assertions.assertEquals(users.get(0), userController.getUser(Mockito.anyLong()).getBody().getBody());
	}

	@Test
	void testGetUserBodyNegative() {
		Mockito.when(userService.getUser(Mockito.anyLong())).thenReturn(users.get(0));
		Assertions.assertNotEquals(users.get(1), userController.getUser(Mockito.anyLong()).getBody().getBody());
	}

	@Test
	void testDeleteUserStatus() throws Exception {
		Mockito.when(userService.getUser(Mockito.anyLong())).thenReturn(users.get(0));
		Mockito.doNothing().when(userService).deleteUser(Mockito.anyLong());
		userService.deleteUser(Mockito.anyLong());
		this.mockMvc.perform(delete("/users/1")).andExpect(status().isOk());
	}

	@Test
	void testDeleteUser() {
		Mockito.when(userService.getUser(Mockito.anyLong())).thenReturn(users.get(0));
		Mockito.doNothing().when(userService).deleteUser(Mockito.anyLong());
		userService.deleteUser(Mockito.anyLong());
		Mockito.verify(userService).deleteUser(Mockito.anyLong());
	}

	@Test
	void testAddUser() throws Exception {
		Mockito.when(userService.addUser(users.get(0))).thenReturn(users.get(0));
		Mockito.when(userService.validateUser(users.get(0))).thenReturn(true);
		mockMvc.perform(post("/users").contentType("application/json").content(asJsonString(users.get(0))))
				.andExpect(status().isCreated());
	}

	@Test
	void testAddUserBadRequest() throws Exception {
		Mockito.when(userService.validateUser(users.get(0))).thenReturn(false);
		mockMvc.perform(post("/users").contentType("application/json").content(asJsonString(users.get(0))))
				.andExpect(status().isBadRequest());
	}

	/*
	 * @Test void testUpdateUser() throws Exception {
	 * Mockito.when(userService.updateUser(users.get(0))).thenReturn(users.get(0));
	 * Mockito.when(userService.validateUser(users.get(0))).thenReturn(true);
	 * mockMvc.perform(patch("/users").contentType("application/json").content(
	 * asJsonString(users.get(0)))) .andExpect(status().isOk()); }
	 * 
	 * @Test void testUpdateUserBadRequest() throws Exception {
	 * Mockito.when(userService.validateUser(users.get(0))).thenReturn(false);
	 * mockMvc.perform(patch("/users").contentType("application/json").content(
	 * asJsonString(users.get(0)))) .andExpect(status().isBadRequest()); }
	 */

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
